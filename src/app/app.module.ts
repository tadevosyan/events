import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ClickEventsComponent} from './click-events.component';
import { KeyboardEvents } from './keyboard-events';
import { UserComponent } from './user.component';


@NgModule({
  declarations: [
    AppComponent,
    ClickEventsComponent,
    KeyboardEvents,
    UserComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
