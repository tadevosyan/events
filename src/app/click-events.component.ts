import { Component } from '@angular/core'


@Component({
    selector: 'click',
    template: `
        <h3>Click Events</h3>
        <button (click)="fireEvent($event, 'hello')">Click Event</button>
        <hr  />
        <h3>MouseOver Event</h3>
        <button (mouseover)="fireEvent($event)">MouseOver</button>
        <hr  />
        <h3>MouseUp Event</h3>
        <button (mouseup)="fireEvent($event)">MouseUp</button>
        <hr  />
        <h3>MouseDown Event</h3>
        <button (mousedown)="fireEvent($event)">MouseDown</button>
        <hr  />
    `
  })
  export class ClickEventsComponent {
    fireEvent(e) {
        console.log(e.type)
    }
  }

