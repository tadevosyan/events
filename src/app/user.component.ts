import { Component } from '@angular/core'

@Component({
selector: 'user-form',
template: `
  <h2>User Form</h2>
  <div class="form-group">
  <input #newName
    (keyup.enter)="addName(newName.value); newName.value=''"
    class="form-control" 
    placeholder="Your Name">
    <br>
  <input #newUser
  (keyup.enter)="addUser(newUser.value)"
  (blur)="addUser(newUser.value); newUser.value='' " class="form-control"
  placeholder="Username">
  <br>
  <button (click)="addUser(newUser.value)" (click)="addName(newName.value)" class="btn btn-success">Sumbit</button>
  </div>
  <br>
  <ul><li *ngFor="let name of names">Your Name: {{name}}</li></ul>  
  <ul><li *ngFor="let user of users">Username: {{user}}</li></ul>
`,
  styles :[`
  ul {
    float:left;
    list-style-type:none;
    font-size:20px;
  }
  `
    
  ],
})




export class UserComponent {
names = ['Poxos Poxosyan', 'Petros Petrosyan'];
  addName(newName: string) {
    if (newName) {
        "Your" + this.names.push(newName);
    }
  }

users = ['First User', 'Second User'];
  addUser(newUser: string) {
    if (newUser) {
        "Username" + this.users.push(newUser);
    }
  }
}


