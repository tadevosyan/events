import { Component } from '@angular/core'


@Component({
    selector: 'keyboard-events',
    template: `
        <h2>Keyborad events</h2>
        <input (keyup)="fireEvent($event)" placeholder="keyup">
        <input (keydown)="fireEvent($event)" placeholder="keydown">
        <input (focus)="fireEvent($event)" placeholder="focus">
        <input (blur)="fireEvent($event)" placeholder="blur">
        <input (cut)="fireEvent($event)" placeholder="cut">
        <input (copy)="fireEvent($event)" placeholder="copy">
        <input (paste)="fireEvent($event)" placeholder="paste">
        <hr  />
    `
  })
  export class KeyboardEvents {
    fireEvent(e) {
        console.log(e.type)
    }
  }

  